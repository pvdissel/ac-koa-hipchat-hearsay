(function ($) {

  "use strict";

  var authToken = $('meta[name="auth-token"]').attr('content');
  var localBaseUrl = $('meta[name="local-base-url"]').attr('content');
  var listenersUrl = localBaseUrl + '/configure/listeners';

  var hearsay = angular.module('hearsay', [], function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
    $httpProvider.interceptors.push(function () {
      return {
        response: function (response) {
          authToken = response.headers('jwt');
          return response;
        }
      };
    });
    $httpProvider.defaults.headers.common = {
      Authorization: 'JWT token=' + authToken
    };
  });

  hearsay.controller('ConfigCtrl', function ($scope, $http, $q, $timeout, $log) {

    $scope.listeners = [];

    loadListeners();

    $scope.addListener = function () {
      $scope.listener = {
        id: Date.now(),
        name: 'New Listener',
        pattern: '(?i)\\btrigger phrase\\b',
        responses: '',
        _dirty: true
      };
      $scope.listeners.push($scope.listener);
      $timeout(function () {
        $('input[name=name]').focus().select();
      });
    };

    $scope.saveListener = function (listener) {
      var promise;
      var validation = validatePattern(listener.pattern);
      if (!validation.valid) {
        return alert(validation.reason);
      }
      if (listener.key) {
        promise = updateListener(listener);
      } else {
        promise = createListener(listener);
      }
      promise.success(function (json) {
        if (json.error) {
          return alert(messageFromServerError(json.error));
        }
        var listener = jsonToListener(json);
        listener._dirty = false;
        replaceListener(listener);
      }).error(function (err) {
        // TODO: display a better error message
        alert(messageFromServerError(err));
      });
    };

    $scope.deleteListener = function (listener) {
      if (confirm('Do you really want to delete this listener? This action can\'t be undone.')) {
        if (listener.key) {
          deleteListener(listener).success(function () {
            removeListener(listener);
          });
        } else {
          removeListener(listener);
        }
      }
    };

    $scope.isValidListener = function (listener) {
      function isValid(field) { return field && $.trim(field).length > 0; }
      return listener && isValid(listener.name) && isValid(listener.pattern) && isValid(listener.responses);
    };

    $scope.showListener = function (listener) {
      $scope.listener = listener;
    };

    $scope.isActiveListener = function (listener) {
      return $scope.listener === listener;
    };

    $scope.isDirtyListener = function (listener) {
      return listener._dirty;
    };

    $scope.listenerChanged = function () {
      $scope.listener._dirty = true;
    };

    $scope.patternFocused = function ($event, listener) {
      if (/^\(\?i\)\\b[^\\]+\\b$/.test(listener.pattern)) {
        $timeout(function () {
          $($event.target).selectRange(6, listener.pattern.length - 2);
        });
      }
    };

    function loadListeners() {
      return $http.get(listenersUrl)
        .success(function (body) {
          $scope.listeners = body.map(function (json) {
            return jsonToListener(json);
          });
          $scope.listener = $scope.listeners[0];
        })
        .error(function (err) {
          // TODO: display an error message
          console.error(err);
        });
    }

    function createListener(listener) {
      var body = listenerToJson(listener);
      return $http.post(listenersUrl, body);
    }

    function updateListener(listener) {
      var body = listenerToJson(listener);
      return $http.put(listenersUrl + '/' + listener.key, body);
    }

    function deleteListener(listener) {
      return $http.delete(listenersUrl + '/' + listener.key)
        .error(function (err) {
          // TODO: display an error message
          console.error(err);
        });
    }

    function removeListener(listener) {
      $scope.listeners = $.map($scope.listeners, function (item) {
        return item.id === listener.id ? null : item;
      });
      $scope.listener = $scope.listeners[0];
    }

    function listenerToJson(listener) {
      var json = {};
      $.each(listener, function (k, v) {
        if (k.charAt(0) !== '_' && k.charAt(0) !== '$') {
          json[k] = listener[k];
        }
      });
      json.responses = $.trim(json.responses).split('\n').map(function (line) {
        return $.trim(line);
      }).filter(function (line) {
        return !!line;
      });
      return json;
    }

    function jsonToListener(json) {
      var listener = {};
      $.each(json, function (k, v) {
        listener[k] = json[k];
      });
      listener.responses = listener.responses ? $.trim(listener.responses.join('\n')) : [];
      return listener;
    }

    function replaceListener(listener) {
      $scope.listeners = $.map($scope.listeners, function (item) {
        return item.id === listener.id ? listener : item;
      });
      if (!$scope.listener || $scope.listener.id === listener.id) {
        $scope.listener = listener;
      }
    }

    function validatePattern(pattern) {
      var match = /\(\?([gimy])+\)(.*)/.exec(pattern);
      var flags;
      if (match) {
        flags = match[1];
        pattern = match[2];
      }
      try {
        new RegExp(pattern, flags);
        return {valid: true};
      } catch (err) {
        return {valid: false, reason: err.message};
      }
    }

  });

  function messageFromServerError(err) {
    if (typeof err === 'string') {
      var index = err.lastIndexOf(':');
      return err.slice(index >= 0 ? index + 1 : 0).trim();
    } else {
      return err.stack || err;
    }
  }

  $.fn.selectRange = function(start, end) {
    return this.each(function() {
      if (this.setSelectionRange) {
        this.focus();
        this.setSelectionRange(start, end);
      } else if (this.createTextRange) {
        var range = this.createTextRange();
        range.collapse(true);
        range.moveEnd('character', end);
        range.moveStart('character', start);
        range.select();
      }
    });
  };

}(AJS.$));

var track = require('ac-koa-hipchat-keenio').track;
var parser = require('koa-body')();
var assets = require('koa-static');
var hbs = require('koa-hbs');

var ack = require('ac-koa').require('hipchat');
var pkg = require('./package.json');
var app = ack(pkg);

app.use(assets(__dirname + '/assets'));
app.use(hbs.middleware({viewPath: __dirname + '/views'}));

var addon = app.addon()
  .hipchat()
  .allowRoom(true)
  .scopes('send_notification', 'admin_room')
  .configurable('/configure');

track(addon);

addon.onWebhook('room_message', function *() {
  var listener = yield this.tenantStore.get(this.query.name);
  if (!listener) return;
  var responses = listener.responses;
  if (!responses || responses.length === 0) return;
  var message = responses[Math.floor(Math.random() * responses.length)];
  message = message.replace(/((?:^|[^\\]))\$(\d)/g, function ($0, $1, $2) {
    var group = this.match[$2 >>> 0];
    return group ? ($1 || '') + group : $0;
  }.bind(this));
  message = message.replace(/\\(\$\d)/g, '$1');
  this.roomClient.sendNotification(message, {
    color: 'gray',
    format: 'text'
  });
});

addon.get('/configure',
  addon.authenticate(),
  function *() {
    yield this.render('configure', this.locals);
  }
);

addon.get('/configure/listeners',
  addon.authenticate(),
  function *() {
    if (this.accepts('json')) {
      // TODO: 1000000 added to work around bug in ac-node's redis store
      var listeners = yield this.tenantStore.all(1000000);
      this.body = Object.keys(listeners).map(function (key) {
        return listeners[key];
      });
    }
  }
);

addon.post('/configure/listeners',
  addon.authenticate(),
  parser,
  function *() {
    if (this.is('json') && this.accepts('json')) {
      var listener = this.request.body;
      validateListener.call(this, listener);
      try {
        var webhook = yield this.tenantWebhooks.add('room_message', listener.pattern);
        listener.key = webhook.name;
        yield this.tenantStore.set(listener.key, listener);
        this.body = listener;
      } catch (err) {
        this.body = {error: err.message || err};
      }
    }
  }
);

addon.put('/configure/listeners/:listenerKey',
  addon.authenticate(),
  parser,
  function *() {
    if (this.is('json') && this.accepts('json')) {
      var listenerKey = this.params.listenerKey;
      var listener = this.request.body;
      validateListener.call(this, listener);
      try {
        var previous = yield this.tenantStore.get(listenerKey);
        if (!previous) this.throw(404);
        if (listener.pattern !== previous.pattern) {
          yield this.tenantWebhooks.remove(listenerKey);
          yield this.tenantStore.del(listenerKey);
          var webhook = yield this.tenantWebhooks.add('room_message', listener.pattern);
          listenerKey = listener.key = webhook.name;
        }
        yield this.tenantStore.set(listenerKey, listener);
        this.body = listener;
      } catch (err) {
        this.body = {error: err.message || err};
      }
    }
  }
);

addon.del('/configure/listeners/:listenerKey',
  addon.authenticate(),
  function *() {
    var listenerKey = this.params.listenerKey;
    var listener = yield this.tenantStore.get(listenerKey);
    if (!listener) this.throw(404);
    yield this.tenantWebhooks.remove(listenerKey);
    yield this.tenantStore.del(listenerKey);
    this.status = 204;
  }
);

function validateListener(listener) {
  if (!listener) {
    this.throw('Input required', 400);
  }
  if (!listener.name || typeof listener.name !== 'string') {
    this.throw('Invalid listener name: ' + listener.name, 400);
  }
  if (!listener.pattern || typeof listener.pattern !== 'string') {
    this.throw('Invalid listener pattern: ' + listener.pattern, 400);
  }
  if (!listener.responses || listener.responses.length === 0) {
    this.throw('Invalid listener responses: ' + listener.responses, 400);
  }
}

app.listen();
